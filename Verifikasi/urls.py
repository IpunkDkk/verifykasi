from django.contrib.auth import logout
from app.viewset_api import cange_note, data_all, data_detail, delete_note , tambah_data , RegisterAPI , LoginAPI, get_note, add_note, note_detail
from django.contrib import admin
from django.contrib.auth.views import LoginView , LogoutView
from django.urls import path , include
from app.views import home , tambahdata , dataview
from rest_framework.urlpatterns import format_suffix_patterns
from knox.views import LogoutAllView

urlpatterns = [
    # api note
    path('api/note' , get_note),
    path('api/note/<int:id>' , note_detail),
    path('api/note-edit/<int:id>' , cange_note),
    path('api/addnote' , add_note),
    path('api/delete-note/<int:id>' , delete_note),
    

    # api verifikasi data
    path('api/logout' , LogoutAllView.as_view()),
    path('api/login', LoginAPI.as_view()),
    path('api/register' , RegisterAPI.as_view()),
    path('api/tambah' , tambah_data),
    path('api/data/' , data_all),
    path('api/data/<int:id>' , data_detail),
    path('admin/', admin.site.urls),
    path('' , home , name='home'),
    path('login/' , LoginView.as_view() , name='login'),
    path('logout/' , LogoutView.as_view(next_page='login') , name='logout'),
    path('tambah-data/' , tambahdata , name='tambah-data'),
    path('list-data' , dataview , name='list-data')
]
urlpatterns = format_suffix_patterns(urlpatterns)