from django.forms import ModelForm
from django import forms
from app.models import Data

class FormData(ModelForm):
    class Meta:
        model = Data
        fields = '__all__'
        widgets = {
            'nama': forms.TextInput({'class':'form-control'}),
            'nik': forms.NumberInput({'class':'form-control'}),
            'nokk': forms.NumberInput({'class':'form-control'}),
        }