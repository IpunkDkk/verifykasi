from django.db import models

# Create your models here.
class Data(models.Model):
    nama = models.CharField(max_length=20)
    nik = models.CharField(max_length=16)
    nokk = models.CharField(max_length=16)
    tglsubmit = models.DateTimeField(auto_now_add=True , null= True)

class User(models.Model):
    username = models.CharField(max_length=20)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    tglsubmit = models.DateTimeField(auto_now_add=True , null= True)

class Inoted(models.Model):
    noteTitle = models.CharField(max_length=200)
    note = models.CharField(max_length=1000)
    tglsubmit = models.DateField(auto_now_add=True, null=True)