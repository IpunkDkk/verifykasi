from django.db.models import fields
from app.models import Data , Inoted
from django.contrib.auth.models import User
from rest_framework import serializers



class DataSerializers(serializers.ModelSerializer):
    class Meta:
        model = Data
        fields = ['id' , 'nama' , 'nik' , 'nokk']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')

class DaftarSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])

        return user

class InotedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inoted
        fields = ('id' , 'noteTitle' , 'note')