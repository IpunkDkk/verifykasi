from django.shortcuts import render
from django.contrib import messages
from app.form import FormData
from app.models import Data

# Create your views here.

def home(request):
    return render(request , 'home.html')

def tambahdata(request):
    if request.POST:
        form = FormData(request.POST , request.FILES)
        if form.is_valid():
            form.save()
            form = FormData()
            konteks = {
                'form' : form
            }
            messages.success(request , 'Data Berhasil Ditambahkan')
            return render(request , 'tambah-data.html' , konteks)
    else:
        form = FormData()
        konteks = {
            'form':form
        }
    return render(request , 'tambah-data.html' ,konteks)


def dataview(request):
    data = Data.objects.all()
    judul = "list Data"
    konteks = {
        'data':data,
        'judl':judul
    }
    return render(request , 'list-data.html' , konteks)