from rest_framework import serializers, status , generics , permissions
from rest_framework import response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from app.models import Data, Inoted
from app.serializers import DataSerializers , DaftarSerializer, UserSerializer, InotedSerializer
from knox.models import AuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView
from django.contrib.auth import login



@api_view(['POST'])
def add_note(request):
    serializer = InotedSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_note(request):
    query = Inoted.objects.all()
    serializer = InotedSerializer(query, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def note_detail(request, id):
    if request.method == 'GET':
        query = Inoted.objects.filter(id=id)
        serializer = InotedSerializer(query, many=True)
        return Response(serializer.data)

@api_view(['PUT'])
def cange_note(request, id):
    if request.method == 'PUT':
        query = Inoted.objects.get(id=id)
        serializer = InotedSerializer(query, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.data , status.HTTP_406_NOT_ACCEPTABLE)
        
@api_view(['DELETE'])
def delete_note(request, id):
    try : 
        data = Inoted.objects.get(id=id)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'DELETE':
        data.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)












@api_view(['GET'])
# @permission_classes([permissions.IsAuthenticated])
def data_all(request , format=None):
    if request.method == 'GET':
        queryset = Data.objects.all()
        serializer = DataSerializers(queryset , many=True)
        return Response(serializer.data)


@api_view(['GET'])
# @permission_classes([permissions.IsAuthenticated])
def data_detail(request  ,id ,  format=None):
    try:
        queryset = Data.objects.get(id=id)
    except Data.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = DataSerializers(queryset , many=False)
        return Response(serializer.data)


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def tambah_data(request):
    serializer = DataSerializers(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data , status=status.HTTP_201_CREATED)
    return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class RegisterAPI(generics.GenericAPIView):
    serializer_class = DaftarSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
        "user": UserSerializer(user, context=self.get_serializer_context()).data,
        "token": AuthToken.objects.create(user)[1]
        })


class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)